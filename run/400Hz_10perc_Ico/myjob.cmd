#!/bin/bash

#SBATCH -o /gpfs/scratch/pr94ho/ga25wex3/TransverseEx/400Hz_10perc_Ico/myjob.%j.%N.out
#SBATCH -D /gpfs/scratch/pr94ho/ga25wex3/TransverseEx/400Hz_10perc_Ico
#SBATCH -J 400Hz10p
#SBATCH --get-user-env
#SBATCH --clusters=ivymuc
#SBATCH --ntasks=16
#SBATCH --mail-type=end
#SBATCH --mail-user=haeringer@tfd.mw.tum.de
#SBATCH --export=NONE
#SBATCH --time=30:00:00

source /etc/profile.d/modules.sh

source $HOME/OpenFOAM/OpenFOAM-2.3.1/etc/bashrc

cd /gpfs/scratch/pr94ho/ga25wex3/TransverseEx/400Hz_10perc_Ico


mpiexec  -n $SLURM_NTASKS icoRhoReactingFoamSc -parallel
