/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2014 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "timeVaryingFixedValueFvPatchField.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

// not called in run, decompose and reconstruct
template<class Type>
timeVaryingFixedValueFvPatchField<Type>::timeVaryingFixedValueFvPatchField
(
    const fvPatch& p,
    const DimensionedField<Type, volMesh>& iF
)
:
    fixedValueFvPatchField<Type>(p, iF),
    timeVaryingValue_(),
    meanValue_(p.size())
{}

// not called in run, decompose and reconstruct
template<class Type>
timeVaryingFixedValueFvPatchField<Type>::timeVaryingFixedValueFvPatchField
(
    const fvPatch& p,
    const DimensionedField<Type, volMesh>& iF,
    const Field<Type>& fld
)
:
    fixedValueFvPatchField<Type>(p, iF, fld),
    timeVaryingValue_(),
    meanValue_(p.size())
{}


// this constructor is called while decompose and reconstruct
template<class Type>
timeVaryingFixedValueFvPatchField<Type>::timeVaryingFixedValueFvPatchField
(
    const timeVaryingFixedValueFvPatchField<Type>& ptf,
    const fvPatch& p,
    const DimensionedField<Type, volMesh>& iF,
    const fvPatchFieldMapper& mapper
)
:
    fixedValueFvPatchField<Type>(p, iF),  // bypass mapper
    timeVaryingValue_(ptf.timeVaryingValue_().clone().ptr()),
    meanValue_(ptf.meanValue_, mapper)
{
    // Evaluate since value not mapped
    const scalar t = this->db().time().timeOutputValue();
    fvPatchField<Type>::operator==(meanValue_ + timeVaryingValue_->value(t));
}


// "normal" constructor executed at start of the simulation
template<class Type>
timeVaryingFixedValueFvPatchField<Type>::timeVaryingFixedValueFvPatchField
(
    const fvPatch& p,
    const DimensionedField<Type, volMesh>& iF,
    const dictionary& dict
)
:
    fixedValueFvPatchField<Type>(p, iF),
    timeVaryingValue_(Function1<Type>::New("timeVaryingValue", dict)),
    meanValue_("meanValue", dict, p.size())
{
    if (dict.found("value"))
    {
        fvPatchField<Type>::operator==(Field<Type>("value", dict, p.size()));
    }
    else
    {
        const scalar t = this->db().time().timeOutputValue();
	fvPatchField<Type>::operator==(meanValue_ + timeVaryingValue_->value(t));
    }
}


// not called in run, decompose and reconstruct
template<class Type>
timeVaryingFixedValueFvPatchField<Type>::timeVaryingFixedValueFvPatchField
(
    const timeVaryingFixedValueFvPatchField<Type>& ptf
)
:
    fixedValueFvPatchField<Type>(ptf),
    timeVaryingValue_
    (
        ptf.timeVaryingValue_.valid()
      ? ptf.timeVaryingValue_().clone().ptr()
      : NULL
    ),
    meanValue_(ptf.meanValue_)
{}


// called within the Pimple loop each time U is accessed
template<class Type>
timeVaryingFixedValueFvPatchField<Type>::timeVaryingFixedValueFvPatchField
(
    const timeVaryingFixedValueFvPatchField<Type>& ptf,
    const DimensionedField<Type, volMesh>& iF
)
:
    fixedValueFvPatchField<Type>(ptf, iF),
    timeVaryingValue_
    (
        ptf.timeVaryingValue_.valid()
      ? ptf.timeVaryingValue_().clone().ptr()
      : NULL
    ),
    meanValue_(ptf.meanValue_)
{
    // For safety re-evaluate
    const scalar t = this->db().time().timeOutputValue();
    if (ptf.timeVaryingValue_.valid())
    {
        fvPatchField<Type>::operator==(meanValue_ + timeVaryingValue_->value(t));
    }
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

// mapping functions: needed for propper decompose and reconstruct!
template<class Type>
void timeVaryingFixedValueFvPatchField<Type>::autoMap
(
    const fvPatchFieldMapper& m
)
{
    fixedValueFvPatchField<Type>::autoMap(m);
    meanValue_.autoMap(m);
}


template<class Type>
void timeVaryingFixedValueFvPatchField<Type>::rmap
(
    const fvPatchField<Type>& ptf,
    const labelList& addr
)
{
    fixedValueFvPatchField<Type>::rmap(ptf, addr);

    const timeVaryingFixedValueFvPatchField<Type>& tiptf =
        refCast<const timeVaryingFixedValueFvPatchField<Type> >(ptf);

    meanValue_.rmap(tiptf.meanValue_, addr);
}


template<class Type>
void timeVaryingFixedValueFvPatchField<Type>::updateCoeffs()
{
    if (this->updated())
    {
        return;
    }

    const scalar t = this->db().time().timeOutputValue();
    fvPatchField<Type>::operator==(meanValue_ + timeVaryingValue_->value(t));
    fixedValueFvPatchField<Type>::updateCoeffs();
}


template<class Type>
void timeVaryingFixedValueFvPatchField<Type>::write(Ostream& os) const
{
    fvPatchField<Type>::write(os);
    meanValue_.writeEntry("meanValue", os);
    timeVaryingValue_->writeData(os);
    this->writeEntry("value", os);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
