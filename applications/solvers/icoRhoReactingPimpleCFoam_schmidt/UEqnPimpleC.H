// Solve the Momentum equation

MRF.correctBoundaryVelocity(U);

tmp<fvVectorMatrix> tUEqn
(
        fvm::ddt(rho, U)
      + fvm::div(phi, U)
      + turbulence->divDevRhoReff(U)
     ==
        fvc::reconstruct(fvc::interpolate(rho*g) & mesh.Sf())
      + fvOptions(rho, U)
);

fvVectorMatrix& UEqn = tUEqn.ref();

UEqn.relax();

fvOptions.constrain(UEqn);

if (pimple.momentumPredictor())
{
    solve
    (
        UEqn
       ==
//             -fvc::grad(p)
        fvc::reconstruct
        (
	       - fvc::snGrad(p)*mesh.magSf()
        )
    );

    fvOptions.correct(U);
}
