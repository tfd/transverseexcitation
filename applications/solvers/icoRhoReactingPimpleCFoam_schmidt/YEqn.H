// tmp<fv::convectionScheme<scalar> > mvConvection
// (
//     fv::convectionScheme<scalar>::New
//     (
//         mesh,
//         fields,
//         phi,
//         mesh.divScheme("div(phi,Yi_h)")
//     )
// );

{
    reaction->correct();
    dQ = reaction->Sh();
	Info<< "min/max(dQ) = " << min(dQ).value() << ", " << max(dQ).value() << endl;
	
    label inertIndex = -1;
    volScalarField Yt(0.0*Y[0]);

    forAll(Y, i)
    {
        if (Y[i].name() != inertSpecie)
        {
		if (i==0)//(Y[i].name() == word("CH4"))      
        		{Si = scalar(0.68);}                 
                                             
		else if (i==4)//(Y[i].name() == word("CO2")) 
	        	{Si = scalar(0.98);}                                                             
		else if (i==2)//(Y[i].name() == word("CO"))  
	        	{Si = scalar(0.76);}                 
		else if (i==1)//(Y[i].name() == word("O2"))  
        		{Si = scalar(0.76);}                
		else if (i==3)//(Y[i].name() == word("H2O")) 
		        {Si = scalar(0.6);}                  
		else if (i==5)//(Y[i].name() == word("N2"))  
        		{Si = scalar(0.75);}
		else                                         
		{                                            
        		Si = scalar(1);                      
		}                                            

            volScalarField& Yi = Y[i];

            fvScalarMatrix YiEqn
            (
                fvm::ddt(rho, Yi)
             //  + mvConvection->fvmDiv(phi, Yi)
			  + fvm::div(phi, Yi)
		      - fvm::laplacian(turbulence->muEff()/Si, Yi)
             ==
                reaction->R(Yi)
              + fvOptions(rho, Yi)
            );

            YiEqn.relax();

            fvOptions.constrain(YiEqn);

            YiEqn.solve(mesh.solver("Yi"));

            fvOptions.correct(Yi);

            Yi.max(0.0);
            Yt += Yi;
        }
        else
        {
            inertIndex = i;
        }
    }

    Y[inertIndex] = scalar(1) - Yt;
    Y[inertIndex].max(0.0);
}
